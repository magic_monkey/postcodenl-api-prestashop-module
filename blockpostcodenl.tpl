<!-- Block Postcodenl -->
<link rel="stylesheet" type="text/css" href="{$module_dir}blockpostcodenl.css" media="screen">
<script>
var debugPcnl = {$debug};
var country_nl_id = {$country_nl_id};
var module_dir = '{$module_dir}';


{literal}
$(document).ready(function() {
	$('#address1').parent().before($('#blockpostcodenl'));
		
	// If selected country is 'nl'
	if($('#id_country').val() == country_nl_id)
	{
		showBlock();
		autoFill();
	}
	else
	{
		hideBlock();
		manualFill();
	}	
	
	// If country selectbox changes
	$('#id_country').change(function(){
		if($(this).val() == country_nl_id)
		{
			showBlock();
			autoFill();
		}
		else
		{
			hideBlock();
			manualFill();
		}
	});
	
	// If manual checkbox changes
	$('#pcnl_handmatig').click(function(){
		if($(this).is(':checked'))
		{
			manualFill();	
		}
		else
		{
			autoFill();
		}
	});	
	
	// Show/hide autofill
	function autoFill(){
		
		$('.pcnl_autocomplete').show();
		
		$('#address1').parent().hide();
		$('#postcode').parent().hide();
		$('#city').parent().hide();
	}
	function manualFill(){
		$('.pcnl_autocomplete').hide();
		
		$('#address1').parent().show();
		$('#postcode').parent().show();
		$('#city').parent().show();		
	}
	
	// Show/hide block
	function showBlock(){
		$('#blockpostcodenl').show();
	}	
	function hideBlock(){
		$('#blockpostcodenl').hide();
	}
	
	// Delay function
	var delay = (function(){
		var timer = 0;
		return function(callback, ms){
			clearTimeout (timer);
			timer = setTimeout(callback, ms);
		};
	})();	
	

	
	$('.pcnl').keyup(function(){
		var postcode = $('#pcnl_postcode').val();
		var huisnummer = $('#pcnl_huisnummer').val();		
		var toevoeging = $('#pcnl_toevoeging').val();	
		
		// If at least postcode and housenumber are filled
		if(postcode.length >= 6 && huisnummer.length != 0)
		{	

			delay(function(){
				$.ajax({
					url: module_dir+'blockpostcodenl-ajax.php',
					type: 'POST',
					dataType: 'json',
					data: { 
						postcode: postcode, 
						huisnummer: huisnummer, 
						toevoeging: toevoeging
					},
					success: function(data) {
						$('#address1').val(data.result['street'] +' '+data.result['houseNumber']+' '+data.result['houseNumberAddition']);
						$('#city').val(data.result['city']);
						$('#postcode').val(data.result['postcode']);
						$('#blockpostcodenl_results').html(data.result['street'] +' '+data.result['houseNumber']+' '+data.result['houseNumberAddition']+'<br>'+data.result['postcode']+'<br>'+data.result['city']);
						
						
				
						if(data.result['message'])
						{
							$('#blockpostcodenl_results').html('<p class="pcnl_error">'+data.result['message']+'</p>');
						}
						
						// If debug is on and console is available
						if(debugPcnl && typeof console == "object")
						{
							console.log(data);
						}
					}
				});	
			}, 300);

		}
	});	
	
});

{/literal}
</script>

<div id="blockpostcodenl">
<h3>{l s='Address validation' mod='blockpostcodenl'}</h3>
<p>	
	{l s='Fill out your postcode and housenumber to auto-complete your address. You can also manually set your address information.' mod='blockpostcodenl'}
</p>

<div class="pcnl_autocomplete">
	<p>
		<label>{l s='Postcode' mod='blockpostcodenl'}</label><input type='text' id='pcnl_postcode' class='pcnl' value=''/> 
	</p>
	<p>
		<label>{l s='Housenumber' mod='blockpostcodenl'}</label><input type='text' id='pcnl_huisnummer' class='pcnl' value=''/> 
	</p>
	<p>
		<label>{l s='Housenumber addition' mod='blockpostcodenl'}</label><input type='text' id='pcnl_toevoeging' class='pcnl' value=''/>
	</p>
	<p>
		<span id="blockpostcodenl_results"></span>
	</p>
</div>
	
<p>
	<label>{l s='Fill out address manually' mod='blockpostcodenl'}</label><input type='checkbox' id='pcnl_handmatig' value='1'/>
</p>
</div>

<!-- /Block Postcodenl -->