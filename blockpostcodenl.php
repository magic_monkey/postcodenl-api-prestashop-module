<?php
if (!defined('_PS_VERSION_'))
	exit;
	
class blockpostcodenl extends Module
{
	public function __construct()
	{
		$this->name = 'blockpostcodenl';
		if(version_compare(_PS_VERSION_, '1.4.0.0') >= 0)
			$this->tab = 'front_office_features';
		else
			$this->tab = 'Blocks';
		$this->version = '1.0';
		$this->author = 'Leon Juffermans - SiteBySite.nl';

		parent::__construct();

		$this->displayName = $this->l('Block Postcode.nl');
		$this->description = $this->l('Postcode.nl postcode complementation and validation.');
	}
	
	public function install()
	{
		return (parent::install() AND $this->registerHook('createAccountForm'));
	}
	
	public function uninstall()
	{
		//Delete configuration			
		return (parent::uninstall() AND $this->unregisterHook(Hook::get('createAccountForm')));
	}
	
	public function getContent()
	{
		$output = '<h2>'.$this->displayName.'</h2>';
		if (Tools::isSubmit('submitBlockPostcodenl'))
		{
			if (!$pcnlKey = Tools::getValue('pcnlKey') OR empty($pcnlKey) OR !$pcnlSecret = Tools::getValue('pcnlSecret') OR empty($pcnlSecret))
			{
				$output .= '<div class="alert error">'.$this->l('You should fill in all fields').'</div>';
			}
			else
			{
				Configuration::updateValue('BLOCKPCNL_KEY', $pcnlKey);
				Configuration::updateValue('BLOCKPCNL_SECRET', $pcnlSecret);
				Configuration::updateValue('BLOCKPCNL_DEBUG', Tools::getValue('pcnlDebug'));
				$output .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="'.$this->l('Confirmation').'" />'.$this->l('Settings updated').'</div>';
			}
		}
		return $output.$this->displayForm();
	}	
	
	public function displayForm()
	{
		$output = '
		<form action="'.$_SERVER['REQUEST_URI'].'" method="post">
			<fieldset><legend><img src="'.$this->_path.'logo.gif" alt="" title="" />'.$this->l('Settings').'</legend>
				<label>'.$this->l('API Key').'</label>
				<div class="margin-form">
					<input type="text" name="pcnlKey" value="'.Configuration::get('BLOCKPCNL_KEY').'" />
					<p class="clear">'.$this->l('Set the API key to connect with the Postcode.nl API').'</p>
				</div>				
				<label>'.$this->l('API Secret').'</label>
				<div class="margin-form">
					<input type="text" name="pcnlSecret" value="'.Configuration::get('BLOCKPCNL_SECRET').'" />
					<p class="clear">'.$this->l('Set the API secret to connect with the Postcode.nl API').'</p>
				</div>
				<label>'.$this->l('Debug').'</label>
				<div class="margin-form">
					'.$this->l('On').' <input type="radio" name="pcnlDebug" value="1" '.(Configuration::get('BLOCKPCNL_DEBUG') == 1 ? 'checked=checked' : '').'/> '.$this->l('Off').' <input type="radio" name="pcnlDebug" value="0" '.(Configuration::get('BLOCKPCNL_DEBUG') == 0 ? 'checked=checked' : '').'/>
					<p class="clear">'.$this->l('Set debug mode on or off').'</p>
				</div>					
				<center><input type="submit" name="submitBlockPostcodenl" value="'.$this->l('Save').'" class="button" /></center>
			</fieldset>
		</form>';
		return $output;
	}	
	
	public function hookCreateAccountForm($params)
	{
		global $smarty, $cookie;		
		
		$smarty->assign(array(
			'country_nl_id' => Country::getByIso('nl'),
			'debug' => Configuration::get('BLOCKPCNL_DEBUG')
		));
			
		return $this->display(__FILE__, 'blockpostcodenl.tpl');
	}
	

	public function ajaxCall()
	{
		global $cookie, $smarty;		
		
		$postcode = Tools::getValue('postcode');
		$huisnummer = Tools::getValue('huisnummer');
		$toevoeging = Tools::getValue('toevoeging');

		$url = 'https://api.postcode.nl/rest/addresses/';

		$ch = curl_init();
		
		$optsSet = array( 
			CURLOPT_URL => $url . $postcode .'/'. $huisnummer . '/'. $toevoeging,
			CURLOPT_RETURNTRANSFER => true, 
			CURLOPT_CONNECTTIMEOUT => 3,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_USERPWD => Configuration::get('BLOCKPCNL_KEY') .':'. Configuration::get('BLOCKPCNL_SECRET')
		);
		
		curl_setopt_array($ch, $optsSet);


		$jsonResponse = json_decode(curl_exec($ch), true);

		
		if(Configuration::get('BLOCKPCNL_DEBUG'))
		{
			$sendResponse['debug']	= array(
				'info' => curl_getinfo($ch),
				'error' => curl_error($ch)
			);
		}	
		curl_close($ch);
		

		
		$sendResponse['result'] = array(
			'street' => $jsonResponse['street'],
			'houseNumber' => $jsonResponse['houseNumber'],		
			'houseNumberAddition' => $jsonResponse['houseNumberAddition'],				
			'city' =>  $jsonResponse['city'],
			'postcode' =>  $jsonResponse['postcode']
		);

		if(isset($jsonResponse['exceptionId']))
		{
			switch ($jsonResponse['exceptionId'])
			{
				case 'PostcodeNl_Controller_Address_InvalidPostcodeException':
					$sendResponse['result']['message'] = $this->l('Invalid postcode format, use `1234AB` format.');
					break;
				case 'PostcodeNl_Service_PostcodeAddress_AddressNotFoundException':
					$sendResponse['result']['message'] = $this->l('Unknown postcode + housenumber combination.');
					break;
				default:
					$sendResponse['result']['message'] = $this->l('Validation error, please use manual input.');
					break;
			}		
		}		
		$sendResponse['result'] = array_map(utf8_encode, $sendResponse['result']);
			
		$smarty->assign(array(
			'result' => json_encode($sendResponse)
			//'default_lang' => (int)$cookie->id_lang,
			//'id_lang' => $cookie->id_lang
		));	
		return $this->display(__FILE__, 'blockpostcodenl-ajax.tpl');
	}
	
}
?>